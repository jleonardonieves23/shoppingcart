package com.test.shopping.cart.repositories;

import org.springframework.data.repository.CrudRepository;

import com.test.shopping.cart.entities.Car;

public interface CarRepository extends CrudRepository<Car, Long>{

}
