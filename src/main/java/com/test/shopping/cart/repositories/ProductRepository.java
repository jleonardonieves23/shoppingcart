package com.test.shopping.cart.repositories;

import org.springframework.data.repository.CrudRepository;

import com.test.shopping.cart.entities.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

}
