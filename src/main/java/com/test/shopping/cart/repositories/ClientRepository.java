package com.test.shopping.cart.repositories;

import org.springframework.data.repository.CrudRepository;

import com.test.shopping.cart.entities.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
