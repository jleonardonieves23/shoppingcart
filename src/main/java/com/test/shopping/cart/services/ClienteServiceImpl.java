package com.test.shopping.cart.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.test.shopping.cart.entities.Client;
import com.test.shopping.cart.repositories.ClientRepository;

@Service
public class ClienteServiceImpl implements IClienteService { 
	
	private ClientRepository clientRepository;
	
	public ClienteServiceImpl(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}

	@Override
	public List<Client> getAllClients() throws Exception {
		List<Client> clients = new ArrayList<>();
		clientRepository.findAll().forEach(clients :: add);
		
		return clients;
	}

	@Override
	public Client getByIdCliente(Long clienteId) throws Exception {
		// TODO Auto-generated method stub		
		Client client = clientRepository.findById(clienteId).orElseThrow(() -> new Exception("Client does not exists"));
	
		return client;
	}

	@Override
	public Client saveClient(Client client) throws Exception {
		// TODO Auto-generated method stub
		return clientRepository.save(client);
	}

	@Override
	public void deteleCliente(Long clienteId) throws Exception {
		// TODO Auto-generated method stub
		Client client = getByIdCliente(clienteId);
		
		clientRepository.delete(client);
	}

}
