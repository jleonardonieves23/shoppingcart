package com.test.shopping.cart.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.test.shopping.cart.entities.Product;

public interface IProductService {

	@Transactional(readOnly = true)
	public List<Product> getAllProducts() throws Exception;

	@Transactional(readOnly = true)
	public Product getByIdProduct(Long idProduct) throws Exception;

	@Transactional
	public Product saveProduct(Product product) throws Exception;

	@Transactional
	public void deleteProduct(Long idProduct) throws Exception;

}
