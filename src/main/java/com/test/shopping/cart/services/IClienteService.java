package com.test.shopping.cart.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.test.shopping.cart.entities.Client;

public interface IClienteService {
	
	@Transactional(readOnly = true)
	public List<Client> getAllClients() throws Exception;
	
	@Transactional(readOnly = true)
	public Client getByIdCliente(Long clienteId) throws Exception;
	
	@Transactional
	public Client saveClient(Client client) throws Exception;

	@Transactional
	public void deteleCliente(Long clienteId) throws Exception;

}
