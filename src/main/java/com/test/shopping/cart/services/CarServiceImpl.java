package com.test.shopping.cart.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.test.shopping.cart.entities.Car;
import com.test.shopping.cart.repositories.CarRepository;

@Service
public class CarServiceImpl implements ICarService {

	private CarRepository carRepository;

	public CarServiceImpl(CarRepository carRepository) {
		this.carRepository = carRepository;
	}

	@Override
	public List<Car> getAllCar() throws Exception {
		List<Car> cars = new ArrayList<>();
		carRepository.findAll().forEach(cars::add);
		return cars;
	}

	@Override
	public Car getByIdCar(Long idCar) throws Exception {
		Car car = carRepository.findById(idCar).orElseThrow(() -> new Exception("Car does not exists"));
		return car;
	}

	@Override
	public Car addNewCar(Car car) throws Exception {

		String status = car.getStatus().toString();

		if (status.equals("LOAD")) {
			List<Car> cars = getAllCar();

			Long id = (long) ((cars.size() == 0) ? 1 : cars.size() + 1);

			car.setId(id);
			return car;
		}

		Car newCar = carRepository.save(car);

		return newCar;
	}

	@Override
	public Car updateCar(Car car, Long idCar) throws Exception {
		// TODO Auto-generated method stub

		Car car2 = getByIdCar(idCar);

		String status = car.getStatus().toString();

		if (status.equals("LOAD")) {
			return car2;
		}

		return carRepository.save(car);
	}

	@Override
	public void deleteCar(Long idCar) throws Exception {
		// TODO Auto-generated method stub
		Car c = getByIdCar(idCar);

		carRepository.delete(c);
	}

}
