package com.test.shopping.cart.services;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.test.shopping.cart.entities.Car;

public interface ICarService {

	@Transactional(readOnly = true)
	public List<Car> getAllCar() throws Exception;

	@Transactional(readOnly = true)
	public Car getByIdCar(Long idCar) throws Exception;

	@Transactional
	public Car addNewCar(Car car) throws Exception;
	
	@Transactional
	public Car updateCar(Car car, Long idCar) throws Exception;

	@Transactional
	public void deleteCar(Long idCar) throws Exception;

}
