package com.test.shopping.cart.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.test.shopping.cart.entities.Product;
import com.test.shopping.cart.repositories.ProductRepository;

@Service
public class ProductServiceImpl implements IProductService {

	private ProductRepository productRepository;

	public ProductServiceImpl(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@Override
	public List<Product> getAllProducts() throws Exception {
		// TODO Auto-generated method stub
		List<Product> products = new ArrayList<>();
		productRepository.findAll().forEach(products::add);
		
		return products;
	}

	@Override
	public Product getByIdProduct(Long idProduct) throws Exception {
		// TODO Auto-generated method stub
		Product product = productRepository.findById(idProduct)
				.orElseThrow(() -> new Exception("Product does not exists"));
		return product;
	}

	@Override
	public Product saveProduct(Product product) throws Exception {
		// TODO Auto-generated method stub
		return productRepository.save(product);
	}

	@Override
	public void deleteProduct(Long idProduct) throws Exception {
		// TODO Auto-generated method stub
		Product product2 = getByIdProduct(idProduct);
		productRepository.delete(product2);

	}

}
