package com.test.shopping.cart.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.shopping.cart.entities.Product;
import com.test.shopping.cart.services.IProductService;

@RestController
public class ProductController {

	@Autowired
	private IProductService productService;

	@GetMapping("/product")
	public ResponseEntity<List<Product>> listProduct() throws Exception {
		List<Product> products = productService.getAllProducts();
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

	@GetMapping("/product/{idProduct}")
	public ResponseEntity<Product> getProduct(@PathVariable("idProduct") Long idProduct) throws Exception {
		Product product = productService.getByIdProduct(idProduct);
		return new ResponseEntity<>(product, HttpStatus.OK);
	}

	@PostMapping("/product")
	public ResponseEntity<Product> savePorduct(@RequestBody Product product) throws Exception {
		Product productNew = productService.saveProduct(product);
		return new ResponseEntity<>(productNew, HttpStatus.CREATED);
	}

	@PutMapping("/product/{idProduct}")
	public ResponseEntity<Product> updateProduct(@PathVariable("idProduct") Long idProduct,

			@RequestBody Product productEdit) throws Exception {
		Product product = productService.saveProduct(productEdit);
		return new ResponseEntity<>(product, HttpStatus.CREATED);
	}

	@DeleteMapping("/product/{idProduct}")
	ResponseEntity<String> deleteProduct(@PathVariable("idProduct") Long idProduct) throws Exception {
		productService.deleteProduct(idProduct);
		return new ResponseEntity<>("Product delete", HttpStatus.OK);
	}

}