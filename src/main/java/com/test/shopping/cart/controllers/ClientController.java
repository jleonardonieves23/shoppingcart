package com.test.shopping.cart.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.shopping.cart.entities.Client;
import com.test.shopping.cart.services.IClienteService;

@RestController
public class ClientController {

	@Autowired
	private IClienteService clienteService;

	@GetMapping("/client")
	public ResponseEntity<List<Client>> listClient() throws Exception {
		List<Client> clients = clienteService.getAllClients();
		return new ResponseEntity<>(clients, HttpStatus.OK);
	}

	@GetMapping("/client/{clienteId}")
	public ResponseEntity<Client> getClient(@PathVariable("clienteId") Long clienteId) throws Exception {
		Client client = clienteService.getByIdCliente(clienteId);
		return new ResponseEntity<>(client, HttpStatus.OK);
	}

	@PostMapping("/client")
	public ResponseEntity<Client> saveClient(@RequestBody Client client) throws Exception {
		Client clientNew = clienteService.saveClient(client);
		return new ResponseEntity<>(clientNew, HttpStatus.CREATED);
	}

	@PutMapping("/client/{clienteId}")
	public ResponseEntity<Client> updateClient(@PathVariable("clienteId") Long clienteId, @RequestBody Client clientEdit)
			throws Exception {
		clienteService.getByIdCliente(clienteId);
		Client client = clienteService.saveClient(clientEdit);
		return new ResponseEntity<>(client, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/client/{clienteId}")
	public ResponseEntity<String> deteleCliente(@PathVariable("clienteId") Long clienteId) throws Exception {
		clienteService.deteleCliente(clienteId);		
		return new ResponseEntity<>("Client delete", HttpStatus.OK);
	}

}
