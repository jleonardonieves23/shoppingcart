package com.test.shopping.cart.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.shopping.cart.entities.Car;
import com.test.shopping.cart.services.ICarService;

@RestController
public class CarController {

	@Autowired
	private ICarService carService;

	@GetMapping("/car")
	public ResponseEntity<List<Car>> listCar() throws Exception {
		List<Car> cars = carService.getAllCar();
		return new ResponseEntity<>(cars, HttpStatus.OK);
	}

	@GetMapping("/car/{idCar}")
	public ResponseEntity<Car> getCar(@PathVariable("idCar") Long idCar) throws Exception {
		Car car = carService.getByIdCar(idCar);
		return new ResponseEntity<>(car, HttpStatus.OK);
	}

	@PostMapping("/car/new")
	public ResponseEntity<Car> saveCar(@RequestBody Car car) throws Exception {
		Car carNew = carService.addNewCar(car);
		return new ResponseEntity<>(carNew, HttpStatus.CREATED);
	}

	@PutMapping("/car/{idCar}")
	public ResponseEntity<Car> updateCar(@PathVariable("idCar") Long idCar, @RequestBody Car car) throws Exception {
		Car carEdit = carService.updateCar(car, idCar);
		return new ResponseEntity<>(carEdit, HttpStatus.CREATED);
	}

	@DeleteMapping("/car/{idCar}")
	public ResponseEntity<String> deleteCar(@PathVariable("idCar") Long idCar) throws Exception { 
		carService.deleteCar(idCar);
		return new ResponseEntity<>("Car delete", HttpStatus.OK);
	}

}
